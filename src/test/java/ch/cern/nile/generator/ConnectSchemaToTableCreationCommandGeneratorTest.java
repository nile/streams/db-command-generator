package ch.cern.nile.generator;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConnectSchemaToTableCreationCommandGeneratorTest {

    public Map<String, Object> data;

    @BeforeEach
    void setUp() {
        data = new HashMap<>();
        data.put("byte_col", (byte) 1);
        data.put("short_col", (short) 2);
        data.put("int_col", 3);
        data.put("long_col", (long) 4);
        data.put("float_col", 5.0f);
        data.put("double_col", 6.0);
        data.put("boolean_col", true);
        data.put("string_col", "test");
        data.put("timestamp_col", 1501834166000L);
        data.put("date_col", new Date());
        data.put("bytes_col", new byte[]{1, 2, 3});
    }

    @Test
    public void generateOracleCreationCommand_GeneratesCommand_ForCorrectFields() {
        String tableName = "test_table";
        List<String> expectedFields = Arrays.asList(
                "byte_col NUMBER(3)",
                "short_col NUMBER(5)",
                "int_col NUMBER(10)",
                "long_col NUMBER(19)",
                "float_col FLOAT",
                "double_col FLOAT(126)",
                "boolean_col NUMBER(1)",
                "string_col VARCHAR2(510)",
                "timestamp_col TIMESTAMP",
                "date_col DATE",
                "bytes_col BLOB"
        );

        String actualCommand =
                ConnectSchemaToTableCreationCommandGenerator.getTableCreationCommand(data, DbType.ORACLE, tableName);

        for (String expectedField : expectedFields) {
            assertTrue(actualCommand.contains(expectedField),
                    "Field " + expectedField + " is missing in the creation command");
        }
    }

}
