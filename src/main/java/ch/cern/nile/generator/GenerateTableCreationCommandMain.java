package ch.cern.nile.generator;


import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonParser;

import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;

import io.kaitai.struct.KaitaiStruct;

/**
 * Main class to generate a table creation command from a packet.
 */
@SuppressWarnings({"checkstyle:BanSystemOut", "PMD.SystemPrintln"})
public final class GenerateTableCreationCommandMain {


    private static final String TABLE_NAME = "<CHANGE_ME>"; // Name of the table to be created

    private static final DbType DB_TYPE = DbType.ORACLE;
    // Database type to generate the command for (currently only Oracle is supported)

    private static final String FRAME = "{\"data\":\"<CHANGE_ME>\"}";
    // Encoded message from the topic to be used for schema inference (e.g. "{\"data\":\"QCIAAQAAAAAAAA4=\"}")

    /**
     * The default fields that are always present in the schema.
     */
    private static final Map<String, Class<?>> DEFAULT_FIELDS = Map.ofEntries(
            Map.entry("DEVICENAME", String.class),
            Map.entry("TIMESTAMP", Long.class),
            Map.entry("APPLICATIONID", Integer.class),
            Map.entry("APPLICATIONNAME", String.class),
            Map.entry("DEVEUI", String.class)
    );

    /**
     * Any additional fields that are not present in the schema.
     * Feel free to add any additional fields that you want to here.
     * <p>
     * e.g.
     * Map.entry("FRAMETYPE", String.class)
     * Map.entry("DATA", String.class)
     */
    private static final Map<String, Class<?>> ADDITIONAL_FIELDS = Map.ofEntries();

    private static final Map<Class<?>, Object> DEFAULT_VALUES = Map.ofEntries(
            Map.entry(byte.class, (byte) 0),
            Map.entry(Byte.class, (byte) 0),
            Map.entry(short.class, (short) 0),
            Map.entry(Short.class, (short) 0),
            Map.entry(int.class, 0),
            Map.entry(Integer.class, 0),
            Map.entry(long.class, 0L),
            Map.entry(Long.class, 0L),
            Map.entry(float.class, 0.0f),
            Map.entry(Float.class, 0.0f),
            Map.entry(double.class, 0.0d),
            Map.entry(Double.class, 0.0d),
            Map.entry(boolean.class, false),
            Map.entry(Boolean.class, false),
            Map.entry(String.class, ""),
            Map.entry(java.sql.Timestamp.class, new java.sql.Timestamp(System.currentTimeMillis())),
            Map.entry(java.sql.Date.class, new java.sql.Date(System.currentTimeMillis())),
            Map.entry(byte[].class, new byte[0])
    );

    /**
     * Replace the `KaitaiStruct.class` with the class of the packet that you want to generate the command for.
     */
    private static final Map<String, Object> PACKET =
            KaitaiPacketDecoder.decode(JsonParser.parseString(FRAME).getAsJsonObject().get("data"), KaitaiStruct.class);

    private GenerateTableCreationCommandMain() {
    }


    /**
     * Instantiates fields that are not present in the packet.
     *
     * @param fields fields to instantiate
     * @return map of instantiated fields
     */
    private static Map<String, Object> instantiateFields(final Map<String, Class<?>> fields) {
        final Map<String, Object> fieldInstances = new HashMap<>();
        for (final Map.Entry<String, Class<?>> entry : fields.entrySet()) {
            final Object defaultValue = DEFAULT_VALUES.get(entry.getValue());
            if (defaultValue != null) {
                fieldInstances.put(entry.getKey(), defaultValue);
            }
        }
        return fieldInstances;
    }

    /**
     * Generates a table creation command for the given packet.
     *
     * @param args not used
     */
    public static void main(final String[] args) {
        PACKET.putAll(instantiateFields(DEFAULT_FIELDS));
        PACKET.putAll(instantiateFields(ADDITIONAL_FIELDS));
        final String tableCreationCommand =
                ConnectSchemaToTableCreationCommandGenerator.getTableCreationCommand(PACKET, DB_TYPE, TABLE_NAME);
        System.out.println(tableCreationCommand);
    }

}
