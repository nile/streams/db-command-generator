package ch.cern.nile.generator;


import java.util.List;
import java.util.Map;

import ch.cern.nile.common.schema.SchemaInjector;


/**
 * Generates a database schema creation command from a Connect schema.
 */
public final class ConnectSchemaToTableCreationCommandGenerator {

    private ConnectSchemaToTableCreationCommandGenerator() {
    }

    /**
     * Generates a database schema creation command from a Connect schema.
     *
     * @param data      Data for which to generate the schema
     * @param dbType    Database type
     * @param tableName Table name
     * @return Database schema creation command
     */
    @SuppressWarnings("PMD.TooFewBranchesForASwitchStatement")
    public static String getTableCreationCommand(final Map<String, Object> data, final DbType dbType,
                                                 final String tableName) {
        final List<Map<String, Object>> connectSchemaFields = getSchemaFields(data);
        switch (dbType) {
            case ORACLE:
                return OracleTableCreationCommandGenerator.generateOracleCreationCommand(connectSchemaFields,
                        tableName);
            default:
                throw new IllegalArgumentException("Unsupported database type: " + dbType);
        }
    }

    @SuppressWarnings("unchecked")
    private static List<Map<String, Object>> getSchemaFields(final Map<String, Object> data) {
        return (List<Map<String, Object>>) ((Map<String, Object>) SchemaInjector.inject(data).get("schema")).get(
                "fields");
    }

}
