package ch.cern.nile.generator;

import java.util.List;
import java.util.Map;

import org.apache.kafka.connect.data.Date;
import org.apache.kafka.connect.data.Timestamp;

/**
 * Generates an Oracle database schema creation command from a Connect schema.
 */
public final class OracleTableCreationCommandGenerator {

    private OracleTableCreationCommandGenerator() {
    }

    static String generateOracleCreationCommand(final List<Map<String, Object>> connectSchemaFields,
                                                final String tableName) {
        final StringBuilder oracleCreationCommand = new StringBuilder();
        appendTableCreationStart(oracleCreationCommand, tableName);

        for (final Map<String, Object> field : connectSchemaFields) {
            appendField(oracleCreationCommand, field);
        }

        removeLastComma(oracleCreationCommand);
        appendTableCreationEnd(oracleCreationCommand);

        return oracleCreationCommand.toString();
    }

    private static void appendTableCreationStart(final StringBuilder oracleCreationCommand, final String tableName) {
        oracleCreationCommand.append("CREATE TABLE ").append(tableName).append(" (\n");
    }

    private static void appendField(final StringBuilder oracleCreationCommand, final Map<String, Object> field) {
        final String fieldName = (String) field.get("field");
        final String fieldType = (String) field.get("type");
        final String name = (String) field.getOrDefault("name", "");
        final String oracleDataType = mapFieldTypeToOracleType(fieldType, name);
        oracleCreationCommand.append(fieldName).append(' ').append(oracleDataType).append(",\n");
    }

    private static void removeLastComma(final StringBuilder oracleCreationCommand) {
        oracleCreationCommand.setLength(oracleCreationCommand.length() - 2);
    }

    private static void appendTableCreationEnd(final StringBuilder oracleCreationCommand) {
        oracleCreationCommand.append("\n)");
    }

    @SuppressWarnings("PMD.CyclomaticComplexity")
    private static String mapFieldTypeToOracleType(final String fieldType, final String name) {
        final String oracleDataType;
        if (Timestamp.LOGICAL_NAME.equals(name)) {
            oracleDataType = "TIMESTAMP";
        } else if (Date.LOGICAL_NAME.equals(name)) {
            oracleDataType = "DATE";
        } else {
            switch (fieldType) {
                case "int8":
                    oracleDataType = "NUMBER(3)";
                    break;
                case "int16":
                    oracleDataType = "NUMBER(5)";
                    break;
                case "int32":
                    oracleDataType = "NUMBER(10)";
                    break;
                case "int64":
                    oracleDataType = "NUMBER(19)";
                    break;
                case "float":
                    oracleDataType = "FLOAT";
                    break;
                case "double":
                    oracleDataType = "FLOAT(126)";
                    break;
                case "string":
                    oracleDataType = "VARCHAR2(510)";
                    break;
                case "boolean":
                    oracleDataType = "NUMBER(1)";
                    break;
                case "date":
                    oracleDataType = "DATE";
                    break;
                case "timestamp":
                    oracleDataType = "TIMESTAMP";
                    break;
                case "bytes":
                    oracleDataType = "BLOB";
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported field type: " + fieldType);
            }
        }
        return oracleDataType;
    }
}
